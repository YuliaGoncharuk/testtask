//
//  NewsManager.swift
//  TestTask
//
//  Created by Mobexs on 13.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class NewsManager {
    let parseQueue : DispatchQueue = DispatchQueue(label: "parseQueue", qos: .userInteractive)
    func getNews (link: String, success: @escaping () -> Void, failure: @escaping (_ error: Error?) -> Void) {
        Alamofire.request(link).responseString(queue: parseQueue){ response in
            switch response.result {
            case .success:
                let item = self.processResponse(response: response.result.value!)
                let realm = try! Realm()
                for element in item {
                    try! realm.write {
                        realm.add(element, update: true)
                    }
                }
                DispatchQueue.main.async {
                    success()
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    print(error)
                    failure(error)
                }
            }
        }
    }
    
     func processResponse(response: String) -> [News] {
        return []
    }
    let articleParseQueue : DispatchQueue = DispatchQueue(label: "articleParseQueue", qos: .userInteractive)
    func loadDetailInfo(news: News, success: @escaping () -> Void, failure: @escaping (_ error: Error?) -> Void) {
        Alamofire.request(news.link).responseString(queue: articleParseQueue){ response in
            switch response.result {
            case .success :
                let articleText = self.getArticleText(news: news, response: response.result.value!)
                 DispatchQueue.main.async {
                    let realm = try! Realm()
                    try! realm.write {
                        let ob = try! Realm().objects(News.self).filter("title == %@", news.title)
                        for element in ob {
                            element.article = articleText
                        }
                    }
                }
               DispatchQueue.main.async {
                    success()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    print(error)
                    failure(error)
               }
            }
        }
    }
    
    func getArticleText(news: News, response: String) -> String {
        return ""
    }
}
