//
//  OnlinerByNews.swift
//  TestTask
//
//  Created by Mobexs on 14.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import XMLMapper
import SwiftSoup
import RealmSwift

fileprivate enum Constants {
    static let articleBody = "news-text"
}

class OnlinerByNews: NewsManager {
    
    var news: News!
    override func processResponse(response: String) -> [News] {
        let list = XMLMapper<OnlinerByList>().map(XMLString: response)
        for element in (list?.news)! {
            element.site = NewsTypes.onlinerBy
            element.category = (list?.category)!
        }
        return (list?.news)!
    }
    
    override func getArticleText(news: News, response: String) -> String {
        self.news = news
        let doc: Document  = try! SwiftSoup.parse(String(describing: response))
        
        let art = try! doc.getElementsByClass("news-header__image")
        var imageLink = try! art.attr("style")
        imageLink = String(imageLink.dropLast(3))
        imageLink = String(imageLink.dropFirst(23))
        setImage(imageURL: imageLink)
        let articleBody = try! doc.getElementsByClass(Constants.articleBody)
        return try! articleBody.text()
    }
    
    func setImage(imageURL: String) {
        DispatchQueue.main.async {
            let realm = try! Realm()
            try! realm.write {
                let ob = try! Realm().objects(News.self).filter("title == %@", self.news.title)
                for element in ob {
                    element.image = UIImageView.setImageFromURl(stringImageUrl: imageURL)
                }
            }
        }
    }
}
