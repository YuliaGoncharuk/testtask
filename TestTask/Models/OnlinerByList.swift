//
//  OnlinerByList.swift
//  TestTask
//
//  Created by Mobexs on 14.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import XMLMapper

class OnlinerByList: XMLMappable {
    
    var nodeName: String!
    var news : [News] = []
    var category = ""
    
    required convenience init(map: XMLMap) {
        self.init()
    }
    
    func mapping(map: XMLMap) {
        var mapper: [OnlinerByNewsMapper] = []
        mapper   <- map["channel.item"]
        category <- map["channel.title"]
        news = mapper.map({ (mapper) -> News in
            return mapper.news
        })
    }
}

