//
//  News.swift
//  TestTask
//
//  Created by Mobexs on 13.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftSoup

class News: Object {
    
    @objc dynamic var title = ""
    @objc dynamic var shortDescription = ""
    @objc dynamic var pubDate = ""
    @objc dynamic var link = ""
    @objc dynamic var icon : NSData!
    @objc dynamic var article = ""
    @objc dynamic var image : NSData!
    @objc dynamic var site = ""
    @objc dynamic var category = ""
    @objc dynamic var dateSort: Date!
    
//    required convenience init() {
//        self.init()
//    }
    override static func primaryKey() -> String? {
        return "link"
    }

}


