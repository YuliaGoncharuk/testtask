//
//  OnlinerByNewsMapper.swift
//  TestTask
//
//  Created by Mobexs on 14.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import XMLMapper
import SwiftSoup

class OnlinerByNewsMapper: XMLMappable {
    
    var nodeName: String!
    var news = News()
    var descriptionInfo = ""
    
    required  init(map: XMLMap) {
    }
    
    func mapping(map: XMLMap){
        news.title <- map["title"]
        descriptionInfo <- map["description"]
        news.pubDate <- map["pubDate"]
        news.link  <- map["link"]
        parseDescription()
        convertingDate()
    }
    
    func parseDescription() {
        do {
            let doc: Document  = try! SwiftSoup.parse(String(describing: descriptionInfo))
            let image: Elements = try doc.select("img[src]")
            let srcsStringArray: [String?] = image.array().map { try? $0.attr("src").description }
            if srcsStringArray.count > 0 {
                let imageUrl = (srcsStringArray[0])!
                news.icon = UIImageView.setImageFromURl(stringImageUrl: imageUrl)
            }
            descriptionInfo = try! doc.body()!.text()
            news.shortDescription = String(descriptionInfo.dropLast(15))
    
        } catch Exception.Error(_, let message) {
            print(message)
        } catch {
            print("error")
        }
    }
    
    func convertingDate(){
        news.pubDate = Date.getFormattedDate(date: news.pubDate)
        news.dateSort = Date.convertToDate(date: news.pubDate)
    }
}

