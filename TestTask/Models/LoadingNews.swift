//
//  StartVCModel.swift
//  TestTask
//
//  Created by Mobexs on 08.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import Alamofire
import XMLMapper
import RealmSwift


class LoadingNews {
    static func loadData (link: String, success: @escaping () -> Void, failure: @escaping (_ error: Error?) -> Void) {
        Alamofire.request(link).responseString{ response in
            switch response.result {
            case .success:
                print(response.result.value ?? "no result")
                let item = XMLMapper<NewsCatalog>().map(XMLString: response.result.value!)
                let realm = try! Realm()
                for element in (item?.news)! {
                    element.category = (item?.category)!
                    element.key = element.category + element.link
                    try! realm.write {
                        realm.add(element, update: true)
                    }
                }
                success()
            case .failure(let error):
                print(error)
                failure(error)
            }
    }
}
}
