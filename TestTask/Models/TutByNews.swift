//
//  TutByNews.swift
//  TestTask
//
//  Created by Mobexs on 13.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import Alamofire
import XMLMapper
import RealmSwift
import SwiftSoup

fileprivate enum Constants {
    static let articleBody = "article_body"
}

class TutByNews: NewsManager {
    override func processResponse(response: String) -> [News] {
        let list = XMLMapper<TutByList>().map(XMLString: response)
        for element in (list?.news)! {
            element.site = NewsTypes.tutBy
            element.category = (list?.category)!
        }
        return (list?.news)!
    }
    
    override func getArticleText(news: News, response: String) -> String {
        let doc: Document  = try! SwiftSoup.parse(String(describing: response))
        let articleBody = try! doc.getElementById(Constants.articleBody)
        
        let doc2: Document  = try! SwiftSoup.parse(String(describing: articleBody))
        var articleText = try! doc2.body()!.text()
        articleText = articleText.rightString
        return String(articleText.dropLast())
    }
}
