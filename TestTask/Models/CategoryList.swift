//
//  CategoryList.swift
//  TestTask
//
//  Created by Mobexs on 09.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import RealmSwift

class CategoryList: Object {
    
    @objc dynamic var title = ""
    @objc dynamic var link = ""
    @objc dynamic var site = ""
    @objc dynamic var category = ""
    
    convenience init(title: String, link: String, site: String, category: String){
        self.init()
        self.title = title
        self.link = link
        self.site = site
        self.category = category
    }
    
    override static func primaryKey() -> String? {
        return "link"
    }

}
