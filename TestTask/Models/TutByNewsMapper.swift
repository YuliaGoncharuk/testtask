//
//  TutByNewsMapper.swift
//  TestTask
//
//  Created by Mobexs on 13.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import XMLMapper
import SwiftSoup

class TutByNewsMapper: XMLMappable {

    var nodeName: String!
    var news = News()
    var descriptionInfo = ""
    var iconURL = ""
    var imageURL = ""
    
    required  init(map: XMLMap) {
    }
    
    func mapping(map: XMLMap){
        news.title <- map["title"]
        descriptionInfo <- map["description"]
        news.pubDate <- map["pubDate"]
        news.link  <- map["link"]
        parseDescription()
        convertingDate()
        var mc : NSDictionary = [:]
        mc <- map["enclosure"]
        imageURL = mc["_url"]! as! String
        news.image = UIImageView.setImageFromURl(stringImageUrl: imageURL)
        news.icon = news.image
    }
    
    func parseDescription() {
            let doc: Document  = try! SwiftSoup.parse(String(describing: descriptionInfo))
          //  let image: Elements = try doc.select("img[src]")
            //let srcsStringArray: [String?] = image.array().map { try? $0.attr("src").description }
           // if srcsStringArray.count > 0 {
           //     news.icon = (srcsStringArray[0])!
          //  }
            news.shortDescription = try! doc.body()!.text()
       
    }
    
    func convertingDate(){
        news.pubDate = Date.getFormattedDate(date: news.pubDate)
        news.dateSort = Date.convertToDate(date: news.pubDate)
    }
}
