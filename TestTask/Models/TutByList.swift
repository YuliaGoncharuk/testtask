//
//  TutByList.swift
//  TestTask
//
//  Created by Mobexs on 13.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import XMLMapper

class TutByList: XMLMappable {
    
    var nodeName: String!
    var news : [News] = []
    var category = ""
    required convenience init(map: XMLMap) {
        self.init()
    }
    
    func mapping(map: XMLMap) {
        var mapper: [TutByNewsMapper] = []
        mapper <- map["channel.item"]
        category <- map["channel.title"]
        news = mapper.map({ (mapper) -> News in
            return mapper.news
        })
    }
}
