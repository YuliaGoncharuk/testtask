//
//  CategoryCollectionViewCell.swift
//  TestTask
//
//  Created by Mobexs on 09.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

fileprivate enum Constants {
    static let cellIdentifier = "categoryCell"
    static let selectedColor = "#1DAAB8"
}

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryNameLbl: UILabel!
    
    static let identifier = Constants.cellIdentifier
    
    func setCell(title: String){
        categoryNameLbl.text = title
    }
    
    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            super.isSelected = newValue
            self.backgroundColor = newValue ? UIColor.hexStringToUIColor(hex: Constants.selectedColor) : UIColor.clear
        }
    }
}
