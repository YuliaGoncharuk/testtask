//
//  TableViewCell.swift
//  TestTask
//
//  Created by Mobexs on 30.01.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

fileprivate enum Constants {
    static let identifier = "cell"
}

class NewsTableViewCell : UITableViewCell {
    
    static let identifier = Constants.identifier
    
    @IBOutlet weak var newTitle: UILabel!
    
    @IBOutlet weak var newDate: UILabel!
    
    @IBOutlet weak var newDescription: UILabel!
    
    @IBOutlet weak var newIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setCell(element: News){
        self.newTitle.text = element.title
        self.newDate.text = String(describing: element.pubDate)
        if element.icon != nil{
            self.newIcon.image = UIImage(data: element.icon as Data)
        }
        self.newDescription.text = element.shortDescription
        }
}

