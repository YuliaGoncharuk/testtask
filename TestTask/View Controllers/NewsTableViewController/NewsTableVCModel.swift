//
//  NewsTableVCModel.swift
//  TestTask
//
//  Created by Mobexs on 08.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import RealmSwift

fileprivate enum Constants {
    static let keyFirstOpening = "firstOpening"
}

enum Instances {
    static let tutBy  = TutByNews()
    static let onlinerBy = OnlinerByNews()
}


class NewsTableVCModel {
    var newsArray = try! Realm().objects(News.self)
    var categories = [CategoryList]()
    var newsSite = ""
    enum Instances {
        static let tutBy  = TutByNews()
        static let onlinerBy = OnlinerByNews()
    }
  
    required init() {
        let categoriesInfo = try! Realm().objects(CategoryList.self)
        if categoriesInfo.count == 0 {
            let realm = try! Realm()
            var instance = CategoryList(title: "Главное", link: "https://news.tut.by/rss/index.rss", site: NewsTypes.tutBy, category: "TUT.BY: Новости ТУТ - Главные новости")
            try! realm.write {
                realm.add(instance, update: true)
            }
            categories.append(instance)
            instance = CategoryList(title: "Технологии", link: "https://tech.onliner.by/feed", site: NewsTypes.onlinerBy, category: "Технологии onliner.by")
            try! realm.write {
                realm.add(instance, update: true)
            }
            categories.append(instance)
        } else {
            for i in 0..<categoriesInfo.count {
                categories.append(categoriesInfo[i])
            }
        }
    }

    
    func fillNewsTable(index: Int){
        newsSite = categories[index].site
        newsArray = try! Realm().objects(News.self).filter("category == %@", categories[index].category).sorted(byKeyPath: "dateSort", ascending: false)
        if Reachability.isConnectedToNetwork() == true {
            switch newsSite {
                case NewsTypes.tutBy:
                    Instances.tutBy.getNews(link: categories[index].link, success: { () -> Void in
                        print("-- ready")
                    }) { (error) -> Void in
                    }
            default:
                Instances.onlinerBy.getNews(link: categories[index].link, success: { () -> Void in
                    print("-- ready")
                }) { (error) -> Void in
                }
            }
        }
    }
}
