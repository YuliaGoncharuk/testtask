//
//  NewsDetailsVCModel.swift
//  TestTask
//
//  Created by Mobexs on 22.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

class NewsDetailsVCModel: UICollectionViewCell {
    func makeRequest(news: News, instance: NewsManager, success: @escaping () -> Void, failure: @escaping (_ error: Error?) -> Void){
        if news.article == "" {
            instance.loadDetailInfo(news: news, success: { () -> Void in
                success()
            }) { (error) -> Void in
                failure(error)
            }
        } else {
            success()
        }
    }
}
