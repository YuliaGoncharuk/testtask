//
//  NewsDetailsViewController.swift
//  TestTask
//
//  Created by Mobexs on 07.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
fileprivate enum Constants{
    static let identifier = "NewsDetail"
}

class NewsDetailsViewController: UIViewController {
    
    static let identifier = Constants.identifier
    
    @IBOutlet weak var whiteView: UIView!
    
    @IBOutlet weak var newsTitleLbl: UILabel!
    
    @IBOutlet weak var newsIconImg: UIImageView!
    
    @IBOutlet weak var newsDateLbl: UILabel!
    
    @IBOutlet weak var newsTextLbl: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var news : News!
    
    var instance: NewsManager!
    
    var model = NewsDetailsVCModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNews()
    }

    func showNews() {
        self.whiteView.isHidden = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        model.makeRequest(news: news,instance: instance, success: { () -> Void in
            self.activityIndicator.stopAnimating()
            self.updateUI()
        }) { (error) -> Void in
        }
    }
    
   
    
    func updateUI(){
        self.whiteView.isHidden = true
        newsTitleLbl.text = news.title
        newsDateLbl.text = String(describing: news.pubDate)
        newsTextLbl.text = news.article
        if news.image != nil {
            newsIconImg.image = UIImage(data: news.image as Data)
        }
    }
}
