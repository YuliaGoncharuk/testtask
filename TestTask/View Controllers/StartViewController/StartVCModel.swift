//
//  StartVCModel.swift
//  TestTask
//
//  Created by Mobexs on 14.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit
import RealmSwift

class StartVCModel {
    let realm = try! Realm()
    var categories = [CategoryList]()
    
    required init(){
        let categoriesInfo = try! Realm().objects(CategoryList.self)
        if categoriesInfo.count == 0{
        var instance = CategoryList(title: "Главное", link: "https://news.tut.by/rss/index.rss", site: NewsTypes.tutBy, category: "TUT.BY: Новости ТУТ - Главные новости")
        try! realm.write {
            realm.add(instance, update: true)
        }
        categories.append(instance)
        instance = CategoryList(title: "Технологии", link: "https://tech.onliner.by/feed", site: NewsTypes.onlinerBy, category: "Технологии onliner.by")
        try! realm.write {
            realm.add(instance, update: true)
        }
        categories.append(instance)
        } else {
            for i in 0..<categoriesInfo.count {
                categories.append(categoriesInfo[i])
            }
        }
    }
}
