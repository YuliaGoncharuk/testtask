//
//  ViewController.swift
//  TestTask
//
//  Created by Mobexs on 30.01.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

fileprivate enum Constants {
    static let mainNewsUrl = "https://news.tut.by/rss/index.rss"
    static let identifier = "NewsTableVC"
}

class StartViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let model = StartVCModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        loadData()
    }
    
    func loadData(){
        activityIndicator.startAnimating()
        let instance: NewsManager = TutByNews()
        instance.getNews(link: model.categories[0].link, success: { () -> Void in
          self.activityIndicator.stopAnimating()
          let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.identifier) as! NewsTableViewController
          vc.newsType = self.model.categories[0].site
          self.navigationController?.pushViewController(vc, animated: true)
      }) { (error) -> Void in
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.errorMessage(errorDescription: error as! String)
        }
    }
    
}


