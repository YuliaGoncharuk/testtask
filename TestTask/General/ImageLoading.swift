//
//  ImageLoading.swift
//  TestTask
//
//  Created by Mobexs on 08.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

extension UIImageView {
    static func setImageFromURl(stringImageUrl url: String) -> NSData? {
        var data : NSData?
        if let url = NSURL(string: url) {
              data = NSData(contentsOf: url as URL)
        }
        return data
    }
}
