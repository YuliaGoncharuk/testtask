//
//  AlertVC.swift
//  TestTask
//
//  Created by Mobexs on 12.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

extension UIViewController {
     func errorMessage(errorDescription: String) {
        let alert = UIAlertController(title: "Error", message: errorDescription, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
