//
//  DateConverting.swift
//  TestTask
//
//  Created by Mobexs on 12.02.18.
//  Copyright © 2018 Mobexs. All rights reserved.
//

import UIKit

extension Date {
    static func getFormattedDate(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
        
        let formateDate = dateFormatter.date(from: date)!
        dateFormatter.dateFormat = "E, MMM dd, yyyy. HH:mm"
        
      //  print ("Print :\(dateFormatter.string(from: formateDate))")//Print :02-02-2018
        return dateFormatter.string(from: formateDate)
    }
    
    static func convertToDate(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, MMM dd, yyyy. HH:mm"
        let date2 = dateFormatter.date(from: date)
        return date2!
    }
}
